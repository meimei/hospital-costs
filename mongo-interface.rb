require 'mongo'
require_relative './xslx-parser.rb'

class Connection
  def initialize
    # set logger to fatal
    Mongo::Logger.logger.level = ::Logger::FATAL

    begin
      @client = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'test')
    rescue => error
      puts "Connection to database failed! Error: "
      puts "\n"
      puts error
      puts error.backtrace
    end
  end

  def client
    @client
  end

  def costs
    @client[:costs]
  end
end

conn = Connection.new
db = conn.client.database
parser = Parser.new
parser.read_file(ARGV.to_s.strip.tr('[]', '').tr('""', ''))

workbook = parser.get
puts workbook.info
puts "\n"

puts "inserting into database"
puts parser.find_most_rows + " has " + parser.row_count.to_s + " rows"
puts "\n"

workbook = parser.get
prices = parser.grab_col(/[aA]mount/)
descrip = parser.grab_col(/[dD]escription/)

insert_doc = { 'description' => descrip,
               'price' => prices 
}

result = conn.costs.insert_one( insert_doc )

if result.n == 1
  puts "Document successfully created.\n#{insert_doc}"
else
  puts "Document creation failed."
end
