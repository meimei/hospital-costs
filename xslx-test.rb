require "test/unit"
require_relative './xslx-parser'

class ParserTest < Test::Unit::TestCase
  def select_most_rows
    assert_equal 12189, Parser.largest_size, "The size of the largest sheet should match the size of the largest sheet in our test spreadsheet"
  end
end
