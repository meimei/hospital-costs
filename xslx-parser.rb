require 'roo'

class Parser
  @@workbook = nil
  
  # Method reads in file name and catches system exceptions to provide more helpful errors
  def read_file(filename)
    begin 
      @@workbook = Roo::Spreadsheet.open(filename, extension: :xlsx)
    rescue => error
      puts "Reading file failed! Filename: " + filename
      puts "------ Error ------"
      puts error
      puts error.backtrace
    end
  end

  def find_most_rows 
    workbook = @@workbook
    # Some super weird functionality - calling the sheets method resets the default sheet to the sheet you just called
    # Want to make it super clear that after this point, the default sheet in the worksheet that we're counting the size of is the *first* sheet
    array = workbook.sheets
    largest_sheet = array[0]
    # Preserve the row count for the currently iterating spreadsheet so as not to erroneously change the default spreadsheet by calling sheet
    temp = workbook.last_row
    size = workbook.last_row
    
    array.each do |spreadsheet|
      # Default sheet changes here to most recent sheet
      temp = workbook.sheet(spreadsheet).last_row

      if temp > size
        largest_sheet = workbook.default_sheet
        size = temp
      end
    end
    
    @@workbook.default_sheet = largest_sheet
    return largest_sheet
  end

  def row_count
    return @@workbook.last_row
  end

  def get
    @@workbook
  end

  def grab_col(regex)
    workbook = @@workbook

    col = 1
    do_break = false
    workbook.each do |row|
      puts row.inspect
      col = 1
      row.each do |cell|
        do_break = true if cell.to_s.match(regex)
        break if do_break
        col += 1
      end
      break if do_break
    end

    puts "Found " + regex.to_s + " on col " + col.to_s
    arr = workbook.column(col).select { |i| i != nil }
    return arr
  end
end

#parser = Parser.new
#
## Grab a command line argument for the filename
#parser.read_file(ARGV.to_s.strip.tr('[]', '').tr('""', ''))
#
#workbook = parser.get
#puts workbook.info
#puts "\n"
#
## Pick the sheet with the most rows, as that one is probably the charge master sheet
#puts "-------- Largest Sheet ----------"
#puts parser.find_most_rows + " has " + parser.row_count.to_s + " rows"
#
#puts "\n"
#
#workbook = parser.get
#prices = parser.grab_col(/[aA]mount/)
#descrip = parser.grab_col(/[dD]escription/)
#
#puts descrip[1]
#
#row = 0
#prices.each do |value|
#  puts descrip[row].to_s + "    cost: " + value.to_s
#  row += 1
#end
